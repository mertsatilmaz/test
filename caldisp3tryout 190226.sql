select * from dk_dmap.p_dim_cal limit 10;
select * from dk_dmap.p_dim_cal_2date limit 10;
select * from dk_dmap.p_dim_cal_master limit 10;
select * from temp_dim_cal limit 10;
select * from dk_draw.p_calendar limit 10;


DROP TABLE IF EXISTS dk_dmap.p_dim_cal_2date;
CREATE TABLE dk_dmap.p_dim_cal_2date (
  calid smallint
  , calkey varchar(100)
  , calcatkey varchar(100)

  --Display fields
  , caldisp varchar(100)
  , caldisp2 varchar(100)
  , caldisp3 varchar(100)

  , PRIMARY KEY (calcatkey)
) WITH ( OIDS=FALSE );


--+++++
DROP TABLE IF EXISTS dk_dmap.p_dim_cal_master;
CREATE TABLE dk_dmap.p_dim_cal_master (
  calid smallint
  , calkey varchar(100) NOT NULL
  , calcatkey varchar(100) NOT NULL

  , calidly smallint
  , calidlly smallint
  , calidllly smallint
  --, calidny smallint
  --, calidnny smallint
  --, calidnnny smallint

  --Client-specific fields to define g_calkey():
  , q_wk_no int
  , mnt_nm char(15)
  , q_qtr_id int
  , q_sea_id int
  , q_yr_id int

  --Client-specific fields for joins:
  , q_wk_id date

  --Display fields:
  , caldisp varchar(100) NOT NULL
  , caldisp2 varchar(100) NOT NULL
  , caldisp3 varchar(100) NOT NULL

  , bgndt date
  , enddt date

  --, PRIMARY KEY (calid)
  , UNIQUE (calkey)
) WITH ( OIDS=FALSE );

--caldisp3: Quarters
CREATE OR REPLACE FUNCTION dk_dmap.g_caldisp3(i1 int, i2 text, i3 int, i4 int, i5 int, icat text) RETURNS text AS $$
DECLARE o1 text;
BEGIN
  CASE icat
      WHEN 'Fiscal Year' THEN o1 := ' ';
      WHEN 'Fiscal Half' THEN o1 := ' ';
      WHEN 'Fiscal Quarter' THEN o1 := 'Q' || SUBSTRING(i3::text,5,1);
      WHEN 'Fiscal Month' THEN o1 := ' ';
      WHEN 'Fiscal Week' THEN o1 := ' ';
  END CASE;
  RETURN o1;
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION dk_dmap.populate_calendar(icat text) RETURNS void AS $$
BEGIN

DROP TABLE IF EXISTS temp_dim_cal;
CREATE TEMP TABLE temp_dim_cal (LIKE dk_dmap.p_dim_cal_master INCLUDING INDEXES);

INSERT INTO temp_dim_cal (calkey, calcatkey, q_wk_no, mnt_nm, q_qtr_id, q_sea_id, q_yr_id, q_wk_id, caldisp, caldisp2, caldisp3, bgndt, enddt)
SELECT
  dk_dmap.g_calkey(q_wk_no, mnt_nm, q_qtr_id, q_sea_id, q_yr_id, icat)
  , icat AS calcatkey
  , dk_dmap.g_q_wk_no(q_wk_no, icat)
  , dk_dmap.g_mnt_nm(mnt_nm, icat)
  , dk_dmap.g_q_qtr_id(q_qtr_id, icat)
  , dk_dmap.g_q_sea_id(q_sea_id, icat)
  , dk_dmap.g_q_yr_id(q_yr_id, icat)
  , dk_dmap.g_q_wk_id(q_wk_id, icat)
  , dk_dmap.g_caldisp(q_wk_no, mnt_nm, q_qtr_id, q_sea_id, q_yr_id, icat)
  , dk_dmap.g_caldisp2(q_wk_no, mnt_nm, q_qtr_id, q_sea_id, q_yr_id, icat)
  , dk_dmap.g_caldisp3(q_wk_no, mnt_nm, q_qtr_id, q_sea_id, q_yr_id, icat)
  , MIN(bgndt)
  , MAX(enddt)
FROM dk_draw.p_calendar
GROUP BY 1,2,3,4,5,6,7,8,9,10,11;

DROP TABLE IF EXISTS temp_dim_cal_new;
CREATE TEMP TABLE temp_dim_cal_new (LIKE dk_dmap.p_dim_cal_master INCLUDING INDEXES);

INSERT INTO temp_dim_cal_new
SELECT * FROM temp_dim_cal
WHERE calkey NOT IN (SELECT calkey FROM dk_dmap.p_dim_cal);

--Sub-query makes sure ids are not randomly assigned
INSERT INTO dk_dmap.p_dim_cal
  (calid, calkey, calcatkey
  , q_wk_no, mnt_nm, q_qtr_id, q_sea_id, q_yr_id, q_wk_id
  , bgndt, enddt)
SELECT dk_dmap.g_calid(icat) AS calid, a.*
FROM
  (SELECT calkey, calcatkey
    , q_wk_no, mnt_nm, q_qtr_id, q_sea_id, q_yr_id, q_wk_id
    , bgndt, enddt
  FROM temp_dim_cal_new
  ORDER BY bgndt) a;

--Desc & Display fields are here only --No need to change statement above, which wont change much anyways after initial project setup
UPDATE dk_dmap.p_dim_cal a
SET
  caldisp = b.caldisp
  , caldisp2 = b.caldisp2
  , caldisp3 = b.caldisp3
FROM temp_dim_cal b
WHERE a.calkey = b.calkey;

END;
$$ LANGUAGE plpgsql;

DROP TABLE IF EXISTS dk_dmap.p_dim_cal;
CREATE TABLE dk_dmap.p_dim_cal (LIKE dk_dmap.p_dim_cal_master INCLUDING INDEXES);
ALTER TABLE dk_dmap.p_dim_cal ALTER COLUMN calid SET NOT NULL;
ALTER TABLE dk_dmap.p_dim_cal ADD PRIMARY KEY (calid);

SELECT dk_dmap.populate_calendar('Fiscal Week');
SELECT dk_dmap.populate_calendar('Fiscal Month');
SELECT dk_dmap.populate_calendar('Fiscal Quarter');
SELECT dk_dmap.populate_calendar('Fiscal Half');
SELECT dk_dmap.populate_calendar('Fiscal Year');

SELECT dk_dmap.populate_maps2cal();

SELECT dk_dmap.populate_refcalweekly();


--Fields for display purpose, assigning same field to caldisp & caldisp2 for now
UPDATE dk_dmap.p_dim_cal_2date a
SET caldisp = b.maps2calkey
  , caldisp2 = b.maps2calkey
  , caldisp3 = b.maps2calkey
FROM (SELECT DISTINCT maps2calkey, maps2calcatkey FROM dk_dmap.p_dim_maps2cal_2date_bop) b
WHERE a.calcatkey = b.maps2calcatkey;
